﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public PlayerMovement player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public AudioSource caughtAudio;

    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    bool m_HasAudioPlayed;
    float m_Timer;

    public float timeLeft;

    void Start()
    {
        timeLeft += 1;
    }

    void OnTriggerEnter(Collider other) // exit trigger
    {
        if (other.gameObject == player.gameObject && player.count <= 0)
        {
            m_IsPlayerAtExit = true;
        }
    }

    public void CaughtPlayer() // enemy trigger
    {
        m_IsPlayerCaught = true;
    }

    void Update() // determines the audio and image shown depending on the event triggered
    { 
        if (m_IsPlayerAtExit)
        {
            Debug.Log("Yes"); 
            if (m_IsPlayerAtExit) // new component that i'm trying to figure out
            {
                EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
            }
        }
        else if (m_IsPlayerCaught)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }

        timeLeft -= Time.deltaTime; // new component
        // print("timeLeft:" + timeLeft);
        if (timeLeft <= 0f) // will make sure that the audio and image are displayed
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
    } 

    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource) // determines what ending you get based on what happens
    {
        if (!m_HasAudioPlayed) // will play audio when this event triggers
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;

        if (m_Timer > fadeDuration + displayImageDuration) // restart if you accidentally get caught, quit application if you beat the game
        {
            if (doRestart)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                Application.Quit();
            }
        }
    }
}

// i had to copy and paste this multiple times and resave it multiple times due to errors i kept on getting that were not explained by the console well