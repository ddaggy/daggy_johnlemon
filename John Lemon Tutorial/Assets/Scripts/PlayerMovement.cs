﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using UnityEngine.InputSystem;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    public TextMeshProUGUI countText;
    public TextMeshProUGUI timeText;
    public TextMeshProUGUI instructionsText;
    public int count;
    public float time;
    // public GameEnding timeLeft;

    public bool timerIsRunning = false;
    public bool textDisplay = false;
    public bool invulnerable;
    float invulnerableCooldown;

    Animator m_Animator; // calling a reference to the Animator
    Rigidbody m_Rigidbody;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    AudioSource m_AudioSource;

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>(); // using the Animator reference properly
        m_Rigidbody = GetComponent<Rigidbody>(); // using the Rigidbody reference properly
        m_AudioSource = GetComponent<AudioSource>(); // using the AudioSource reference properly

        timerIsRunning = true; // added this boolean for the online instructions i got
        time += 1;

        SetCountText();
        SetInstructionsText();
    }

    void SetInstructionsText() // there wasn't much that really directed you so i wrote instructions
    {
        instructionsText.text = "Listen up, you just got out of a bank heist, but your buddy dropped all the money during the escape." +
            "You have 5 minutes until the cops catch you. Grab it all. Leave no buck behind." +
            "You may wonder, where are the cops? They went to grab donuts. Act fast.";
    }
    void SetCountText()
    {
        countText.text = "How Many Left: " + count.ToString(); // rather than have count go up, count goes down so you know when you can finish the game
    }

    // Yes, I am once again adding a component to pick up.
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("CollectorItem"))
        {
            other.gameObject.SetActive(false);
            count -= 1;
            SetCountText();
            // Debug.Log("The exit is right up ahead, just walk straight"); // because i coudln't figure out how to make it visible
        }
    }
    
    private void Update() // added component, to make me invisible
    {
        if (timerIsRunning) // got most of this part online, thank you https://gamedevbeginner.com/how-to-make-countdown-timer-in-unity-minutes-seconds/#time_remaining_vs_elapsed
        {
            if (time > 0)
            {
                time -= Time.deltaTime;
                SetTimeText();
            }
            else
            {
                time = 0;
                timerIsRunning = false;
            }

        }

        void SetTimeText() // this is what made the timer work like it should
        {
            float minutes = Mathf.FloorToInt(time / 60); 
            float seconds = Mathf.FloorToInt(time % 60); 

            timeText.text = "Time Left: " + string.Format("{0:00}:{1:00}", minutes, seconds);
        }

        /* // i would've used this had there been cops to actually use them with.
        if (invulnerable) // if you check the debug log, it technically works
        {
            invulnerableCooldown -= Time.deltaTime;
            print("invulnerabilityCooldown:" + invulnerableCooldown);
            if (invulnerableCooldown <= 0f)
            {
                invulnerable = false;
                Debug.Log("You are not immune.");
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && invulnerableCooldown <= 0f)
        {
            invulnerable = true;
            invulnerableCooldown = 5f; // cooldown time
            Debug.Log("You are immune.");
        }
        */
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("isWalking", isWalking);

        if(isWalking) // a boolean expression to make sure that the audio plays when Lemon is moving around
        {
            if(!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f); // what makes Lemon move
        m_Rotation = Quaternion.LookRotation(desiredForward);

    }

    void OnAnimatorMove() // for his actual animation sequence
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }
}
