﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{
    public Transform player;
    public GameEnding gameEnding;

    bool m_IsPlayerInRange;

    void OnTriggerEnter(Collider other) // when player enters trigger zone
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = true;
        }
    }

    void OnTriggerExit(Collider other) // when player exits trigger zone
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = false;
        }
    }

    void Update()
    {
        if (m_IsPlayerInRange)
        {
            Vector3 direction = player.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction); // for the shining red light with the gargoyles, and for the ghosts
            RaycastHit raycastHit; // again for the shining red light and ghosts

            if (Physics.Raycast(ray, out raycastHit))
            {
                if (raycastHit.collider.transform == player)
                {
                    PlayerMovement pMovement = player.GetComponent<PlayerMovement>();
                    if (!pMovement.invulnerable)
                    {
                        gameEnding.CaughtPlayer();
                    }
                }
            }
        }
    }
}

// i had it written down, but then i had to copy and paste it from the tutorial because i was trying to figure out what was wrong
